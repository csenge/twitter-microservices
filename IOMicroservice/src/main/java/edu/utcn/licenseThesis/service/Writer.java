package edu.utcn.licenseThesis.service;

import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by csenge on 25.06.2017.
 */
public class Writer {

    private final static Logger LOGGER = Logger.getLogger(Writer.class.getName());

    public void writeToCsv(String fileName, List<Object[]> rows) {
        FileWriter outputWriter = null;
        try {
            outputWriter = new FileWriter(fileName);
            CsvWriter writer = new CsvWriter(outputWriter, new CsvWriterSettings());
            writer.writeRowsAndClose(rows);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Error occured while writing to file. " + e.getMessage());
        }
    }
}