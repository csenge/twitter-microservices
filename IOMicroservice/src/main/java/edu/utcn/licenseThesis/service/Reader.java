package edu.utcn.licenseThesis.service;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by csenge on 25.06.2017.
 */
public class Reader {

    private final static Logger LOGGER = Logger.getLogger(Reader.class.getName());

    public List<String[]> readFile(String fileName) {
        LOGGER.log(Level.INFO, "Reading the file: " + fileName);
        CsvParserSettings settings = new CsvParserSettings();
        CsvParser parser = new CsvParser(settings);
        try {
            return parser.parseAll(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.WARNING, "File was not found. Error: " + e.getMessage());
        }
        return null;
    }
}