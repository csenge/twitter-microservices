package edu.utcn.licenseThesis.executor;

import edu.utcn.licenseThesis.service.Reader;
import edu.utcn.licenseThesis.service.Writer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Created by csenge on 01.07.2017.
 */
public class RequestExecutor {

    public void write(String fileName, List<Object[]> rows) throws IOException {
        Writer writer = new Writer();
        if (fileName.contains(".csv")) {
            writer.writeToCsv(fileName, rows);

        } else {
            writer.writeToCsv(fileName + ".csv", rows);

        }
    }

    public List<String[]> read(String location, String fileName) throws FileNotFoundException {
        Reader reader = new Reader();
        if (fileName.contains(".csv")) {
            return reader.readFile(location + fileName);
        } else {
            return reader.readFile(location + fileName + ".csv");
        }

    }
}
