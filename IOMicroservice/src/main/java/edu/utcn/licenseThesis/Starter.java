package edu.utcn.licenseThesis;

import edu.utcn.licenseThesis.messaging.Server;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by csenge on 25.06.2017.
 */
public class Starter {

    public static void main(String[] args) throws IOException, TimeoutException {
       Server.waitForRequests();

    }
}
