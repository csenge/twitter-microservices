package edu.utcn.licenseThesis.hander;

import edu.utcn.licenseThesis.executor.RequestExecutor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by csenge on 27.06.2017.
 */
public class MessageHandler {

    RequestExecutor executor = new RequestExecutor();

    public String processMessage(String message) throws IOException {
        String[] messageComponents = message.split(",");

        switch (messageComponents[0]) {
            case "Write":
                List<Object[]> result = new ArrayList<>();
                int size = Integer.parseInt(messageComponents[2]);
                Object[] currentTweetedMessage = new Object[size + 1];

                for (int i = 3; i < messageComponents.length; i++) {
                    currentTweetedMessage[(i - 3) % (size + 1)] = messageComponents[i];
                    if ((i - 3) % (size + 1) == size) {
                        result.add(currentTweetedMessage);
                        currentTweetedMessage = new Object[size + 1];
                    }
                }

                executor.write(messageComponents[1], result);
                return "The file " + messageComponents[1] + "was successfully written to the disk!" ;

            case "Read":
                try {
                     return processReadEntities(executor.read(messageComponents[1], messageComponents[2]));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        return "error";
    }

    private String processReadEntities(List<String[]> readEntities) {
        StringBuilder readEntityStringBuilder = new StringBuilder();
        for (String[] entity : readEntities
                ) {
            for (String entityComponent : entity
                    ) {
                readEntityStringBuilder.append(entityComponent).append(",");
            }
        }
        return readEntityStringBuilder.toString();
    }
}
