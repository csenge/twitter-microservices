package edu.utcn.licenseThesis;

import edu.utcn.licenseThesis.executor.OperationExecutor;
import edu.utcn.licenseThesis.messagining.Server;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by csenge on 25.06.2017.
 */
public class Starter {

    public static void main(String[] args) throws IOException, TimeoutException {
       Server.waitForRequests();
    }
}
