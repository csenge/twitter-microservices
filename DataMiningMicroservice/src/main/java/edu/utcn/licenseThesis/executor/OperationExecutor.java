package edu.utcn.licenseThesis.executor;

import edu.utcn.licenseThesis.entity.BoundingBoxEnum;
import edu.utcn.licenseThesis.entity.CityEnum;
import edu.utcn.licenseThesis.entity.TweetedMessage;
import edu.utcn.licenseThesis.messagining.Client;
import edu.utcn.licenseThesis.service.TweetManager;
import twitter4j.GeoLocation;
import twitter4j.TwitterException;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by csenge on 27.06.2017.
 */
public class OperationExecutor {

    Client writerClient = new Client("ioQueue");

    public OperationExecutor() throws IOException, TimeoutException {
    }

    public void executeSearchByLocation(String city, String timeString) throws TwitterException {
        int time = Integer.parseInt(timeString);
        int currentTime = 0;

        StringBuilder result=  new StringBuilder();

        while (currentTime  <= time * 3) {
            for (TweetedMessage tweetedMessage : TweetManager.getTweetsByLocation(CityEnum.valueOf(city))

                    ) {
                result.append(formatMessage(tweetedMessage));
            }
            try {
                TimeUnit.MINUTES.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        writerClient.sendRequest("Write,"+city+"_"+new Date()+",10,"+result.toString());
    }

    public void executeSearchByKeyword(String topic, String timeString, String cityString) {
        int time = Integer.parseInt(timeString);
        BoundingBoxEnum city = BoundingBoxEnum.valueOf(cityString);

        int currentTime = 0;

       StringBuilder result = new StringBuilder();
        while (currentTime <= time * 3 ) {
            for (TweetedMessage tweetedMessage : TweetManager.getTweets(topic)

                    ) {
                if (null != city && !city.equals("ww")){
                    if ( null != tweetedMessage.getLocation() && inBoundingBox(city, tweetedMessage.getLocation()))
                        result.append(formatMessage(tweetedMessage));
                }else{
                    result.append(formatMessage(tweetedMessage));

                }

            }
        /*    try {
                TimeUnit.MINUTES.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
        }
        writerClient.sendRequest("Write,"+topic+"_"+cityString+"_"+new Date()+",10,"+result.toString());

    }

    public void executeSearchPopularTopics() throws TwitterException {
        StringBuilder result = new StringBuilder();

        for (String topiic : TweetManager.getPopularTopics()
                ) {
            result.append("Write, PopularTopics"+"_"+new Date()+",1,"+formatTopics(topiic));
        }

        writerClient.sendRequest(result.toString());

    }

    private String formatTopics(String message) {
        StringBuilder formattedObject = new StringBuilder();
        formattedObject.append(message).append(",");
        return formattedObject.toString();
    }

    private String formatMessage(TweetedMessage message) {
        StringBuilder formattedObject = new StringBuilder();
        formattedObject.append(message.getId()).append(",");                         //0
        formattedObject.append(message.getTweetedText()).append(",");                //1
        formattedObject.append(message.getHashtags()).append(",");                   //2
        formattedObject.append(message.getCreationDate()).append(",");               //3
        formattedObject.append(message.getFavCount()).append(",");                   //4
        formattedObject.append(message.getLocation().getLatitude()).append(",");                   //5
        formattedObject.append(message.getLocation().getLongitude()).append(",");                   //6
        formattedObject.append(message.getRetweetCount()).append(",");               //7
        formattedObject.append(message.getRetweetStatus()).append(",");              //8
        formattedObject.append(message.getSentiment()).append(",");                  //9
        formattedObject.append(message.getPlace()).append(",");                      //10
        return formattedObject.toString();
    }

    private boolean inBoundingBox(BoundingBoxEnum boundingBox, GeoLocation location){

        return  (boundingBox.getLatitude() < location.getLatitude() && boundingBox.getLatitudeEnd() > location.getLatitude()
                && boundingBox.getLongitude() < location.getLongitude() && boundingBox.getLongitudeEnd() > location.getLongitude()) ? true :false;
    }
}
