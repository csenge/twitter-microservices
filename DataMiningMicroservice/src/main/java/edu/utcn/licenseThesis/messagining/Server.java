package edu.utcn.licenseThesis.messagining;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import edu.utcn.licenseThesis.handler.RequestHandler;
import twitter4j.TwitterException;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by csenge on 25.06.2017.
 */
public class Server {
    private static final String RPC_QUEUE_NAME = "dataMiningQueue";

    private final static Logger LOGGER = Logger.getLogger(Server.class.getName());

    public static void waitForRequests() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        LOGGER.log(Level.INFO, "Created server for Data Mining microservice");
        Connection connection = null;
        try {
            connection      = factory.newConnection();
            final Channel channel = connection.createChannel();

            channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);

            channel.basicQos(1);

            System.out.println(" [x] Awaiting RPC requests");

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                            .Builder()
                            .correlationId(properties.getCorrelationId())
                            .build();
                    String response = "received data!";

                    try {
                        String message = new String(body,"UTF-8");
                        RequestHandler.processReqest(message);

                        System.out.println(" [.] fib(" + message + ")");
                    }
                    catch (RuntimeException e){
                        LOGGER.log(Level.WARNING, "Exception occurred: " + e.getMessage());
                    } catch (TwitterException e) {
                        LOGGER.log(Level.WARNING, "Exception occurred: " + e.getMessage());
                    } catch (TimeoutException e) {
                        LOGGER.log(Level.WARNING, "Exception occurred: " + e.getMessage());
                    } finally {
                        channel.basicPublish( "", properties.getReplyTo(), replyProps, response.getBytes("UTF-8"));

                        channel.basicAck(envelope.getDeliveryTag(), false);
                    }
                }
            };

            channel.basicConsume(RPC_QUEUE_NAME, false, consumer);

            //loop to prevent reaching finally block
            while(true) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException _ignore) {}
            }
        } catch (IOException | TimeoutException e) {
            LOGGER.log(Level.WARNING, "Exception occurred: " + e.getMessage());
        }
        finally {
            if (connection != null)
                try {
                    connection.close();
                } catch (IOException _ignore) {}
        }
    }
}