package edu.utcn.licenseThesis.service;

import edu.utcn.licenseThesis.entity.CityEnum;
import edu.utcn.licenseThesis.entity.TweetedMessage;
import twitter4j.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by cvarga on 12/4/2016.
 */
public class TweetManager {
    private final static Logger LOGGER = Logger.getLogger(TweetManager.class.getName());

    public static List<TweetedMessage> getTweets(String topic) {

        NLP.init();
        Integer i = 0;
        Twitter twitter = new TwitterFactory().getInstance();

        List tweetList = new ArrayList<TweetedMessage>();
        try {
            Query query = new Query(topic);
            QueryResult result;
            do {
                result = twitter.search(query);
                List<Status> tweets = result.getTweets();
                for (Status tweet : tweets) {

                    int senti = NLP.findSentiment(tweet.getText());
                    TweetedMessage message = new TweetedMessage();

                    if (null != tweet.getGeoLocation()) {
                        message.setLocation(tweet.getGeoLocation());

                    } else {
                        message.setLocation(null);
                    }
                    message.setCreationDate(tweet.getCreatedAt());

                    message.setFavCount(tweet.getFavoriteCount());
                    message.setId(i);
                    message.setTweetedText(tweet.getText().replace("\n", "")
                            .replace("\r", "")
                            .replace("\"", "")
                            .replace(",", " "));
                    message.setSentiment(senti);
                    try {
                        message.setPlace(tweet.getPlace().getName());
                    } catch (NullPointerException e) {
                        message.setPlace("");
                        LOGGER.log(Level.INFO, "Place not found for tweeted message");

                    }
                    HashtagEntity[] hashtagEntities = tweet.getHashtagEntities();
                    StringBuilder sb = new StringBuilder();
                    for (HashtagEntity he : hashtagEntities
                            ) {
                        sb.append(he.getText());
                        sb.append(" ");
                    }

                    message.setHashtags(sb.toString());
                    tweetList.add(message);

                    i++;
                }

            }
           while ((query = result.nextQuery()) != null && tweetList.size() < 200 && twitter.getRateLimitStatus().get("/search/tweets").getRemaining() > 1);
        } catch (TwitterException te) {
            System.out.println("Failed to search tweets: " + te.getMessage());
            LOGGER.log(Level.WARNING, "Exception occured while searching: " + te.getMessage());
            return tweetList;
        }

        return tweetList;
    }

    public static List<TweetedMessage> getTweetsByLocation(CityEnum city) throws TwitterException {
        List<TweetedMessage> tweetedMessages = new ArrayList<>();
        Twitter twitter = new TwitterFactory().getInstance();
        Query query = new Query();
        int i = 0;
        GeoLocation geo = new GeoLocation(city.getLatitude(), city.getLongitude());
        query.setGeoCode(geo, 380.0, Query.KILOMETERS);

        QueryResult result = twitter.search(query);
        List<Status> tweets = result.getTweets();
        for (Status tweet : tweets) {
            int senti;
            try {
                senti = NLP.findSentiment(tweet.getText());

            } catch (NullPointerException npe) {
                senti = -11;
                LOGGER.log(Level.INFO, "Could not extract text from tweeted message.");
            }
            TweetedMessage message = new TweetedMessage();

            if (null != tweet.getGeoLocation()) {
                message.setLocation(tweet.getGeoLocation());

            } else {
                message.setLocation(null);
            }
            message.setCreationDate(tweet.getCreatedAt());

            message.setFavCount(tweet.getFavoriteCount());
            message.setId(i);
            message.setTweetedText(tweet.getText().replace("\n", "")
                    .replace("\r", "")
                    .replace("\"", "")
                    .replace(",", " "));
            message.setSentiment(senti);
            try {
                message.setPlace(tweet.getPlace().getName());
            } catch (NullPointerException e) {
                message.setPlace(city.name());
                LOGGER.log(Level.INFO, "Place not found for tweeted message");

            }
            HashtagEntity[] hashtagEntities = tweet.getHashtagEntities();
            StringBuilder sb = new StringBuilder();
            for (HashtagEntity he : hashtagEntities
                    ) {
                sb.append(he.getText());
                sb.append(" ");
            }

            message.setHashtags(sb.toString());
            tweetedMessages.add(message);

            i++;
        }
        return tweetedMessages;
    }

    public static List<String> getPopularTopics() throws TwitterException {
        List<String> allAvaiableTrends = new ArrayList<>();
        Twitter twitter = new TwitterFactory().getInstance();
        ResponseList<Location> locations;
        locations = twitter.getAvailableTrends();
        System.out.println("Showing available trends");
        try {


            for (Location location : locations) {
                System.out.println(location.getName() + " (woeid:" + location.getWoeid() + ")");
                Trends trends = twitter.getPlaceTrends(location.getWoeid());
                Trend[] trends1 = trends.getTrends();
                try {
                    TimeUnit.MINUTES.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < trends1.length; i++) {
                    System.out.println(trends1[i].getName());
                    allAvaiableTrends.add(trends1[i].getName());
                }
            }
        } catch (TwitterException te) {
            LOGGER.log(Level.WARNING, "Exception occurred: " + te.getMessage());

            return allAvaiableTrends;
        }
        return allAvaiableTrends;
    }
}