package edu.utcn.licenseThesis.handler;

import edu.utcn.licenseThesis.executor.OperationExecutor;
import edu.utcn.licenseThesis.service.TweetManager;
import twitter4j.TwitterException;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by csenge on 27.06.2017.
 */
public class RequestHandler {

    public static void processReqest(String requestMessage) throws TwitterException, IOException, TimeoutException {

        OperationExecutor executor = new OperationExecutor();
        String[] requestMessageParts = requestMessage.split("#");

        switch (requestMessageParts[0]) {

            case "SearchByLocation":
                executor.executeSearchByLocation(requestMessageParts[1], requestMessageParts[2]);
                break;
            case "SearchByKeyword":
                executor.executeSearchByKeyword(requestMessageParts[1], requestMessageParts[2], requestMessageParts[3]);
                break;
            case "SearchPopularTopics":
                System.out.println("csf");
                executor.executeSearchPopularTopics();
                break;
            default:
                System.out.println("Invalid");
                break;

        }
    }
}
