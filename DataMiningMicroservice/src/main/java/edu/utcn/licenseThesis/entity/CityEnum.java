package edu.utcn.licenseThesis.entity;

/**
 * Created by csenge on 05.07.2017.
 */
public enum CityEnum {

    NEW_YORK(-74,40),
    LONDON(51.69, -0.5),
    LOS_ANGELES(33.74, -118.18),
    SAN_FRANCISCO(-122.75,36.8);

    private final double longitude;
    private final double latitude;

    CityEnum(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }
}
