package edu.utcn.licenseThesis.entity;

/**
 * Created by csenge on 19.02.2017.
 */
public enum BoundingBoxEnum {

    NEW_YORK(-74,40,-73,41),
    LONDON(51.69, -0.5, 51.27, 0.25),
    LOS_ANGELES(33.74, -118.18, 34.32, -118.58),
    ww(33.74, -118.18, 34.32, -118.58),
    SAN_FRANCISCO(-122.75,36.8,-121.75,37.8);

    private final double longitude;
    private final double latitude;
    private final double longitudeEnd;
    private final double latitudeEnd;

    BoundingBoxEnum(double longitude, double latitude, double longitudeEnd, double latitudeEnd) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.longitudeEnd = longitudeEnd;
        this.latitudeEnd = latitudeEnd;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitudeEnd() {
        return longitudeEnd;
    }

    public double getLatitudeEnd() {
        return latitudeEnd;
    }
}
