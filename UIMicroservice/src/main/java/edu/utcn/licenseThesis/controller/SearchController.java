package edu.utcn.licenseThesis.controller;

import edu.utcn.licenseThesis.messaging.Client;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by csenge on 26.06.2017.
 */

@Controller
@RequestMapping("/search")
public class SearchController {

    Client messageSender = new Client("dataMiningQueue");
    private final static Logger LOGGER = Logger.getLogger(SearchController.class.getName());

    public SearchController() throws IOException, TimeoutException {
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    String home(Model model) {
        model.addAttribute("search", null);
        return "search/search";
    }

    @RequestMapping(value = "/searchKeyword", method = RequestMethod.POST)
    ModelAndView searchKeyword(@RequestParam("title") String title, @RequestParam("timeframe") String timeIntervalString, @RequestParam("chosenCity") String city){
        if (null == title){
            messageSender.sendMessage("SearchByLocation#"+city+"#" +timeIntervalString);
            LOGGER.log(Level.INFO, "Requesting searching for data in  : " + city) ;

        }else{
            messageSender.sendMessage("SearchByKeyword#"+title+"#" +timeIntervalString + "#"+city);
            LOGGER.log(Level.INFO, "Requesting searching for data in  : " + city + "with specified topic: " + title ) ;

        }
        return new ModelAndView("redirect:/search");
    }

    @RequestMapping(value = "/searchPopularTopics", method = RequestMethod.POST)
    ModelAndView searchPopularTopics(){
        messageSender.sendMessage("SearchPopularTopics#");
        LOGGER.log(Level.INFO, "Requesting searching for popular topics worldwide" ) ;
        return new ModelAndView("redirect:/search");
    }

}
