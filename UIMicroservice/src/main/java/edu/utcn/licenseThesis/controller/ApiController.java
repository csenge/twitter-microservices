package edu.utcn.licenseThesis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by csenge on 26.06.2017.
 */

@Controller
public class ApiController {


    @RequestMapping(value = "", method = RequestMethod.GET)
    String home(Model model) {
        model.addAttribute("search", null);
        return "search/home";
    }

    @RequestMapping(value = "/op1", method = RequestMethod.POST)
    ModelAndView search(){
        System.out.println("Search for keyword ");
        return new ModelAndView("redirect:search");
    }

    @RequestMapping(value = "/op2", method = RequestMethod.POST)
    ModelAndView process(){
        System.out.println("Process data");
        return new ModelAndView("redirect:/data");
    }
}
