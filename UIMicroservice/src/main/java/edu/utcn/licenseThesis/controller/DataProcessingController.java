package edu.utcn.licenseThesis.controller;

import edu.utcn.licenseThesis.messaging.Client;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by csenge on 26.06.2017.
 */

@Controller
@RequestMapping("/data")
public class DataProcessingController {

    private final static Logger LOGGER = Logger.getLogger(DataProcessingController.class.getName());

    Client tweetedMessagedProcessingMessageSender = new Client("tweetedMessageProcessingQueue");

    String response = null;
    public DataProcessingController() throws IOException, TimeoutException {
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    String home(Model model) {
        model.addAttribute("search", response);
        return "search/data";
    }


    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    ModelAndView uploadFile(@RequestParam("location") String fileLocation, @RequestParam("name") String fileName){
        System.out.println("uploadFile "+fileName+ " from "+fileLocation);
        response = tweetedMessagedProcessingMessageSender.sendMessage("UploadFile#"+fileLocation+"#"+fileName+"#");
        LOGGER.log(Level.INFO, "Requesting file upload of : "+fileLocation + fileName);
        return new ModelAndView("redirect:/data");
    }

    @RequestMapping(value = "/searchPopularLocations", method = RequestMethod.POST)
    ModelAndView searchPopularLocations(){
        response = tweetedMessagedProcessingMessageSender.sendMessage("GetPopularLocations#");
        LOGGER.log(Level.INFO, "Requesting statistics creation of popular locations. ");

        return new ModelAndView("redirect:/data");
    }

    @RequestMapping(value = "/searchPopularKeywords", method = RequestMethod.POST)
    ModelAndView searchPopularKeywords(){
        response = tweetedMessagedProcessingMessageSender.sendMessage("GetPopularKeywords#");
        LOGGER.log(Level.INFO, "Requesting statistics creation of popular keywords. ");

        return new ModelAndView("redirect:/data");
    }


    @RequestMapping(value = "/filterByGeolocation", method = RequestMethod.POST)
    ModelAndView filterByGeolocation(@RequestParam(name = "latitudeFilter") String latitude, @RequestParam(name = "longitudeFilter") String logitude, @RequestParam(name = "radius") String radius){
        response = tweetedMessagedProcessingMessageSender.sendMessage("FilterByGeo#" + latitude + "#" + logitude + "#" + radius);
        LOGGER.log(Level.INFO, "Requesting filtering by geolocation: "+ latitude + " " + logitude+ " " + radius);

        return new ModelAndView("redirect:/data");
    }

    @RequestMapping(value = "/filterBySentiment", method = RequestMethod.POST)
    ModelAndView filterBySentiment(@RequestParam(name = "sentimentValue") String sentimentValue){
        response = tweetedMessagedProcessingMessageSender.sendMessage("filterBySentiment#" + sentimentValue );
        LOGGER.log(Level.INFO, "Requesting filtering by sentiment: "+ sentimentValue );

        return new ModelAndView("redirect:/data");
    }

    @RequestMapping(value = "/filterByKeyword", method = RequestMethod.POST)
    ModelAndView filterByKeyword(@RequestParam(name = "kwfilter") String keywordValue){
        response = tweetedMessagedProcessingMessageSender.sendMessage("filterByKeyword#" + keywordValue );
        LOGGER.log(Level.INFO, "Requesting filtering by keyword : "+ keywordValue );

        return new ModelAndView("redirect:/data");
    }

    @RequestMapping(value = "/getSentiment", method = RequestMethod.POST)
    ModelAndView getSentiment(){
        response = tweetedMessagedProcessingMessageSender.sendMessage("GetSentiment#");
        LOGGER.log(Level.INFO, "Requesting statistics creation of by sentimens. ");
        return new ModelAndView("redirect:/data");
    }


    @RequestMapping(value = "/getTendency", method = RequestMethod.POST)
    ModelAndView getTendency(){
        response = tweetedMessagedProcessingMessageSender.sendMessage("GetTendency#");
        LOGGER.log(Level.INFO, "Requesting tweeting habits tendency. ");
        return new ModelAndView("redirect:/data");
    }

    @RequestMapping(value = "/removeNoise", method = RequestMethod.POST)
    ModelAndView removeNoise(@RequestParam("chosenRegression") String chosenRegression, @RequestParam(value = "crossValidation1", required = false)  String crossValidation){
        if (null != crossValidation){
            response = tweetedMessagedProcessingMessageSender.sendMessage("RemoveNoise#" + chosenRegression + "#true" );

        }else{
            response =  tweetedMessagedProcessingMessageSender.sendMessage("RemoveNoise#" + chosenRegression + "#false" );

        }
        LOGGER.log(Level.INFO, "Requesting noise removal with  " + chosenRegression);
        System.out.println(crossValidation);
        return new ModelAndView("redirect:/data");
    }


    @RequestMapping(value = "/predict", method = RequestMethod.POST)
    ModelAndView predict(@RequestParam("timeframe") String timeframe, @RequestParam(value = "chosenRegression") String chosenRegression , @RequestParam(value = "crossValidation", required = false)  String crossValidation){
        if (null != crossValidation) {
            response =  tweetedMessagedProcessingMessageSender.sendMessage("Predict#"+timeframe + "#" + chosenRegression +"#true");

        }else{
            response =  tweetedMessagedProcessingMessageSender.sendMessage("Predict#"+timeframe + "#" + chosenRegression + "#false");

        }
        LOGGER.log(Level.INFO, "Requesting data prediction with  " + chosenRegression);
        return new ModelAndView("redirect:/data");
    }

    @RequestMapping(value = "/createDiagram", method = RequestMethod.POST)
    ModelAndView createDiagrams(@RequestParam("frequency") String frequency){
        response = tweetedMessagedProcessingMessageSender.sendMessage("CreateInputForDiagramCreation#"+frequency);
        LOGGER.log(Level.INFO, "Requesting files for diagram creation. " +frequency);
        return new ModelAndView("redirect:/data");
    }

}
