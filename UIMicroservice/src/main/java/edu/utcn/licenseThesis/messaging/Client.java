package edu.utcn.licenseThesis.messaging;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by csenge on 25.06.2017.
 */
public class Client {

    private final static Logger LOGGER = Logger.getLogger(Client.class.getName());

    private Connection connection;
    private Channel channel;
    private final String requestQueueName;
    private String replyQueueName;

    public Client(String requestQueueName) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        this.requestQueueName = requestQueueName;

        try {
            connection = factory.newConnection();
            channel = connection.createChannel();
            replyQueueName = channel.queueDeclare().getQueue();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Exception occurred: " + e.getMessage());
        } catch (TimeoutException e) {
            LOGGER.log(Level.WARNING, "Exception occurred: " + e.getMessage());
        }
    }

    public String call(String message) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();

        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", requestQueueName, props, message.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<String>(1);

        channel.basicConsume(replyQueueName, true, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                if (properties.getCorrelationId().equals(corrId)) {
                    response.offer(new String(body, "UTF-8"));
                }
            }
        });

        return response.take();
    }

    public void close() throws IOException {
        connection.close();
    }

    public String sendMessage(String message) {
        Client clientRpc = null;
        String response = null;
        try {
            clientRpc = new Client(requestQueueName);

            System.out.println(" [x] Requesting " + message);
            LOGGER.log(Level.INFO, "Sending request: " + message);
            response = clientRpc.call(message);
            LOGGER.log(Level.INFO, "Received response: " + response);
            System.out.println(" [.] Got '" + response + "'");
            return response;
        } catch (IOException  | InterruptedException e) {
            LOGGER.log(Level.WARNING, "Exception occurred: " + e.getMessage());         } finally {
            if (clientRpc != null) {
                try {
                    clientRpc.close();
                } catch (IOException _ignore) {
                }
            }
        }
        return response;
    }
}