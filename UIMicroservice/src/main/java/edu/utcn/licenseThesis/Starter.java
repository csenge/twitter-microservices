package edu.utcn.licenseThesis;

import edu.utcn.licenseThesis.messaging.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by csenge on 25.06.2017.
 */

@SpringBootApplication
public class Starter {

    public static void main(String[] args){
        SpringApplication.run(Starter.class, args);
        Server.waitForRequests();

    }
}
