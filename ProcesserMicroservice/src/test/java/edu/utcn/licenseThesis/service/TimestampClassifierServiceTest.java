package edu.utcn.licenseThesis.service;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by csenge on 09.07.2017.
 */
public class TimestampClassifierServiceTest {

    TimestampClassifierService timestampClassifierService = new TimestampClassifierService();

    @Test
    public void formatHourToFiles(){
        String result;
        Date date = new Date();
        date.setHours(14);

        date.setMinutes(02);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:00", result);

        date.setMinutes(06);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:05", result);

        date.setMinutes(12);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:10", result);

        date.setMinutes(18);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:15", result);

        date.setMinutes(22);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:20", result);

        date.setMinutes(27);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:25", result);

        date.setMinutes(31);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:30", result);

        date.setMinutes(39);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:35", result);

        date.setMinutes(44);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:40", result);

        date.setMinutes(46);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:45", result);

        date.setMinutes(54);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:50", result);

        date.setMinutes(59);
        result = timestampClassifierService.formatHourToFives(date);
        assertEquals("14:55", result);
    }
}
