package edu.utcn.licenseThesis.service;

import java.util.Date;

/**
 * Created by csenge on 02.07.2017.
 */
public class TimestampClassifierService {


    public String formatHourToFives(Date dateToClassyfy) {
        StringBuilder time = new StringBuilder();
        time.append(dateToClassyfy.getHours());
        time.append(":");
        int minutes = dateToClassyfy.getMinutes();
        if (minutes >= 0 && minutes <= 5) {
            time.append("00");
            return time.toString();
        } else if (minutes <= 10) {
            time.append("05");
            return time.toString();
        } else if (minutes <= 15) {
            time.append("10");
            return time.toString();
        } else if (minutes <= 20) {
            time.append("15");
            return time.toString();
        } else if (minutes <= 25) {
            time.append("20");
            return time.toString();
        } else if (minutes <= 30) {
            time.append("25");
            return time.toString();
        } else if (minutes <= 35) {
            time.append("30");
            return time.toString();
        } else if (minutes <= 40) {
            time.append("35");
            return time.toString();
        } else if (minutes <= 45) {
            time.append("40");
            return time.toString();
        } else if (minutes <= 50) {
            time.append("45");
            return time.toString();
        } else if (minutes <= 55) {
            time.append("50");
            return time.toString();
        }
        time.append("55");
        return time.toString();
    }

    public String formatHourToTens(Date dateToClassyfy) {
        StringBuilder time = new StringBuilder();
        time.append(dateToClassyfy.getHours());
        time.append(":");
        int minutes = dateToClassyfy.getMinutes();
        if (minutes >= 0 && minutes <= 10) {
            time.append("00");
            return time.toString();
        } else if (minutes <= 20) {
            time.append("10");
            return time.toString();
        } else if (minutes <= 30) {
            time.append("20");
            return time.toString();
        } else if (minutes <= 40) {
            time.append("30");
            return time.toString();
        } else if (minutes <= 50) {
            time.append("40");
            return time.toString();
        }
        time.append("50");
        return time.toString();
    }

    public String formatHourToQuaters(Date dateToClassyfy) {
        StringBuilder time = new StringBuilder();
        time.append(dateToClassyfy.getHours());
        time.append(":");
        int minutes = dateToClassyfy.getMinutes();
        if (minutes >= 0 && minutes <= 15) {
            time.append("00");
            return time.toString();
        } else if (minutes <= 30) {
            time.append("15");
            return time.toString();
        } else if (minutes <= 45) {
            time.append("30");
            return time.toString();
        }
        time.append("45");
        return time.toString();
    }

    public String formatHourToTwenties(Date dateToClassyfy) {
        StringBuilder time = new StringBuilder();
        time.append(dateToClassyfy.getHours());
        time.append(":");
        int minutes = dateToClassyfy.getMinutes();
        if (minutes >= 0 && minutes <= 20) {
            time.append("00");
            return time.toString();
        } else if (minutes <= 40) {
            time.append("20");
            return time.toString();
        }
        time.append("40");
        return time.toString();
    }

    public String formatHourToHalves(Date dateToClassyfy) {
        StringBuilder time = new StringBuilder();
        time.append(dateToClassyfy.getHours());
        time.append(":");
        int minutes = dateToClassyfy.getMinutes();
        if (minutes >= 0 && minutes <= 30) {
            time.append("00");
            return time.toString();
        }
        time.append("30");
        return time.toString();
    }
}
