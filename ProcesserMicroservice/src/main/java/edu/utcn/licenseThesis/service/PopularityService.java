package edu.utcn.licenseThesis.service;

import edu.utcn.licenseThesis.entity.TweetedMessage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by csenge on 29.06.2017.
 */
public class PopularityService {

    private final static Logger LOGGER = Logger.getLogger(PopularityService.class.getName());

    public List<Object[]> createKeywordStatisticsDataSet(List<TweetedMessage> input, String field) {

        LOGGER.log(Level.INFO, "Creating statistics on "+ field);
        List<String> allKeywords = extractFields(input, field);
        List<Object[]> output = new ArrayList<>();
        Set<String> keywordSet = new HashSet<>(allKeywords);
        for (String keyword : keywordSet
                ) {
            int freqvency = countKeywordFreqvency(keyword, allKeywords);
            output.add(new Object[]{keyword, freqvency});
        }
        return output;
    }

    private List<String> extractFields(List<TweetedMessage> input, String field) {
        List<String> fieldValueList = new ArrayList<>();

        switch (field) {
            case ("keyword"):
                for (TweetedMessage currentMessage : input
                        ) {
                    if (null != currentMessage.getHashtags()) {
                        String[] allHashtags = currentMessage.getHashtags().split(" ");
                        for (String hashtagEntity : allHashtags
                                ) {
                            fieldValueList.add(hashtagEntity);
                        }
                    }
                }
                break;
            case ("location"):
                for (TweetedMessage currentMessage : input
                        ) {
                    if (! currentMessage.getPlace().equals("null")){
                        fieldValueList.add(currentMessage.getPlace());
                    }
                }
                break;
            case("sentiment"):
                for (TweetedMessage currentMessage : input
                        ) {
                    fieldValueList.add(String.valueOf(currentMessage.getSentiment()));

                }
                break;
        }
        return fieldValueList;
    }

    private int countKeywordFreqvency(String desiredKeyword, List<String> allKeywords) {
        int count = 0;
        for (String word : allKeywords
                ) {
            if (word.equals(desiredKeyword)) {
                count++;
            }
        }
        return count;
    }
}