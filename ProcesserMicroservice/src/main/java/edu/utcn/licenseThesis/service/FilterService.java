package edu.utcn.licenseThesis.service;

import edu.utcn.licenseThesis.entity.TweetedMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by csenge on 12.07.2017.
 */
public class FilterService {
    public List<Object[]> filterByGeolocation(List<TweetedMessage> uploadedFile, String latitude, String logitude, String radius) {
        List<Object[]> result = new ArrayList<>();
        for (TweetedMessage tm: uploadedFile
             ) {
            if (inBoundingBox(Double.valueOf(latitude), Double.valueOf(logitude), tm.getLatitude(), tm.getLongitude(), Double.valueOf(radius))){
                result.add(new Object[]{tm.getTweetedText(), tm.getCreationDate(), tm.getSentiment()});
            }
        }
        
        return result;
    }

    public List<Object[]> filterBySentiment(List<TweetedMessage> uploadedFile, String sentimentValue) {
        List<Object[]> result = new ArrayList<>();
        for (TweetedMessage tm: uploadedFile
                ) {
            if (Integer.valueOf(sentimentValue) == tm.getSentiment())
            result.add(new Object[]{tm.getTweetedText(), tm.getCreationDate(), tm.getSentiment()});

        }
        return result;
    }

    public List<Object[]> filterByKeyword(List<TweetedMessage> uploadedFile, String keyword) {
        List<Object[]> result = new ArrayList<>();
        for (TweetedMessage tm: uploadedFile
                ) {
            if (tm.getHashtags().contains(keyword))
                result.add(new Object[]{tm.getTweetedText(), tm.getCreationDate(), tm.getSentiment()});

        }
        return result;
    }

    private boolean inBoundingBox(Double boundingBoxLatitude, Double boundingBoxLongitude, double latitude, double longitude, Double radius) {
        if ( Math.abs(boundingBoxLatitude - latitude) <= radius && Math.abs(boundingBoxLongitude -longitude) <= radius){
            return true;
        }
        return false;
    }

}

   
