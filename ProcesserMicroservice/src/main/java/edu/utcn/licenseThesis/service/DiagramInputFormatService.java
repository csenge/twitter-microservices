package edu.utcn.licenseThesis.service;

import edu.utcn.licenseThesis.entity.TweetedMessage;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by csenge on 02.07.2017.
 */
public class DiagramInputFormatService {

    TimestampClassifierService classifierService = new TimestampClassifierService();

    public String formatWith1MinTimestamp(List<TweetedMessage> input) {
        StringBuilder diagramFormat = new StringBuilder();
        for (TweetedMessage message : input
                ) {

            diagramFormat.append(message.getId())
                    .append(",")
                    .append(message.getCreationDate().getHours())
                    .append(message.getCreationDate().getMinutes())
                    .append(",")
                    .append(message.getSentiment())
                    .append(",");

        }
        return diagramFormat.toString();
    }


    public String formatWith5MinTimestamp(List<TweetedMessage> input){
        StringBuilder diagramFormat = new StringBuilder();
        for (TweetedMessage message : input
                ) {
            diagramFormat.append(message.getId())
                    .append(",")
                    .append(classifierService.formatHourToFives(message.getCreationDate()))
                    .append(",")
                    .append(message.getSentiment())
                    .append(",");
        }
        return diagramFormat.toString();

    }

    public String formatWith10MinTimestamp(List<TweetedMessage> input){
        StringBuilder diagramFormat = new StringBuilder();
        for (TweetedMessage message : input
                ) {
            diagramFormat.append(message.getId())
                    .append(",")
                    .append(classifierService.formatHourToTens(message.getCreationDate()))
                    .append(",")
                    .append(message.getSentiment())
                    .append(",");
        }
        return diagramFormat.toString();

    }

    public String formatWith15MinTimestamp(List<TweetedMessage> input){
        StringBuilder diagramFormat= new StringBuilder();
        for (TweetedMessage message : input
                ) {
            diagramFormat.append(message.getId())
                    .append(",")
                    .append(classifierService.formatHourToQuaters(message.getCreationDate()))
                    .append(",")
                    .append(message.getSentiment())
                    .append(",");
        }
        return diagramFormat.toString();

    }

    public String formatWith20MinTimestamp(List<TweetedMessage> input){
        StringBuilder diagramFormat = new StringBuilder();
        for (TweetedMessage message : input
                ) {
            diagramFormat.append(message.getId())
                    .append(",")
                    .append(classifierService.formatHourToTwenties(message.getCreationDate()))
                    .append(",")
                    .append(message.getSentiment())
                    .append(",");
        }
        return diagramFormat.toString();

    }

    public String formatWith30MinTimestamp(List<TweetedMessage> input){
        StringBuilder diagramFormat = new StringBuilder();
        for (TweetedMessage message : input
                ) {
            diagramFormat.append(message.getId())
                    .append(",")
                    .append(classifierService.formatHourToHalves(message.getCreationDate()))
                    .append(",")
                    .append(message.getSentiment())
                    .append(",");
        }
        return diagramFormat.toString();

    }

    public String formatWith60MinTimestamp(List<TweetedMessage> input){
        StringBuilder diagramFormat = new StringBuilder();
        for (TweetedMessage message : input
                ) {
            diagramFormat.append(message.getId())
                    .append(",")
                    .append(message.getCreationDate().getHours())
                    .append(",")
                    .append(message.getSentiment())
                    .append(",");
        }
        return diagramFormat.toString();

    }

}
