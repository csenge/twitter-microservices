package edu.utcn.licenseThesis.service;

import edu.utcn.licenseThesis.entity.TweetedMessage;
import edu.utcn.licenseThesis.entity.TweetedMessageRecord;
import edu.utcn.licenseThesis.handler.MachineLearningInputTransformationHandler;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by csenge on 29.06.2017.
 */
public class MachineLearningService {

    private final static Logger LOGGER = Logger.getLogger(MachineLearningService.class.getName());

    public double computeRegressionTrend(final List<TweetedMessage> uploadedFile) throws ParseException {
        Collection<TweetedMessageRecord<Number>> data = MachineLearningInputTransformationHandler.tweetedMessageToRecordFormat(uploadedFile);
        LOGGER.log(Level.INFO, "Computing regression trend for uploaded input" );

        final SimpleRegression simpleRegression = new SimpleRegression();
        final Iterator<TweetedMessageRecord<Number>> iterator = data.iterator();
        TweetedMessageRecord<Number> currentRecord;
        while (iterator.hasNext()) {
            currentRecord = iterator.next();
            simpleRegression.addData(currentRecord.getTimestamp(), currentRecord.getValue().doubleValue());
        }
        return simpleRegression.getSlope();
    }

    public Collection<TweetedMessageRecord<Number>> predictNextValuesInInterval(List<TweetedMessage> uploadedFile, Long predictionFrame) throws ParseException {
        Collection<TweetedMessageRecord<Number>> data = MachineLearningInputTransformationHandler.tweetedMessageToRecordFormat(uploadedFile);

        Long maxTimpestamp = extractTimestamps(data)
                .stream()
                .mapToLong(v -> v)
                .max().orElseThrow(NoSuchElementException::new);

        for (Long l = maxTimpestamp; l < maxTimpestamp + 100 * predictionFrame; l++) {
            double predicted = predictNextValue(uploadedFile, l);
            data.add(new TweetedMessageRecord<>(l, predicted));
            System.out.println("Predicted " + predicted + "for time " + l);
        }
        LOGGER.log(Level.INFO, "Computing future values using simple regression" );
        return data;
    }

    public Collection<TweetedMessageRecord<Number>> removeNoise(List<TweetedMessage> uploadedFile) throws ParseException {
        Collection<TweetedMessageRecord<Number>> data = MachineLearningInputTransformationHandler.tweetedMessageToRecordFormat(uploadedFile);
        Collection<Long> allTimpstamps = computeAllTimestamps();
        Collection<Long> extractedTimestamps = extractTimestamps(data);

        Long maxTimpestamp = extractedTimestamps
                .stream()
                .mapToLong(v -> v)
                .max().orElseThrow(NoSuchElementException::new);

        Long minTimpestamp = extractedTimestamps
                .stream()
                .mapToLong(v -> v)
                .min().orElseThrow(NoSuchElementException::new);

        for (Long timeStamp : allTimpstamps
                ) {
            if (!extractedTimestamps.contains(timeStamp) && timeStamp > minTimpestamp && timeStamp < maxTimpestamp) {
                int predicatedValue = predictNextValue(uploadedFile, timeStamp);
                data.add(new TweetedMessageRecord<>(timeStamp, predicatedValue));
                System.out.println(timeStamp + " " + predicatedValue);
            }
        }
        LOGGER.log(Level.INFO, "Removing noise from input data using simple regression" );
        return data;


    }

    public Collection<TweetedMessageRecord<Number>> predictNextValuesInIntervalWithOLS(List<TweetedMessage> uploadedFile, Long predictionFrame) throws ParseException {
        Collection<TweetedMessageRecord<Number>> data = MachineLearningInputTransformationHandler.tweetedMessageToRecordFormat(uploadedFile);
        Collection<Long> extractedTimestamps = extractTimestamps(data);

        Long maxTimpestamp = extractedTimestamps
                .stream()
                .mapToLong(v -> v)
                .max().orElseThrow(NoSuchElementException::new);

        for (Long l = maxTimpestamp; l < maxTimpestamp + 100 * predictionFrame; l++) {
            double predicted = predictNextValuesWithOLS(uploadedFile, l);
            data.add(new TweetedMessageRecord<>(l, predicted));
            System.out.println("Predicted " + predicted + "for time " + l);
        }

        LOGGER.log(Level.INFO, "Computing future values using OLS regression" );
        return data;
    }

    public Collection<TweetedMessageRecord<Number>> removeNoiseWithOLS(List<TweetedMessage> uploadedFile) throws ParseException {
        Collection<TweetedMessageRecord<Number>> data = MachineLearningInputTransformationHandler.tweetedMessageToRecordFormat(uploadedFile);
        Collection<Long> allTimpstamps = computeAllTimestamps();
        Collection<Long> extractedTimestamps = extractTimestamps(data);

        Long maxTimpestamp = extractedTimestamps
                .stream()
                .mapToLong(v -> v)
                .max().orElseThrow(NoSuchElementException::new);

        Long minTimpestamp = extractedTimestamps
                .stream()
                .mapToLong(v -> v)
                .min().orElseThrow(NoSuchElementException::new);

        for (Long timeStamp : allTimpstamps
                ) {
            if (!extractedTimestamps.contains(timeStamp) && timeStamp > minTimpestamp && timeStamp < maxTimpestamp) {
                int predicatedValue = (int) predictNextValuesWithOLS(uploadedFile, timeStamp);
                data.add(new TweetedMessageRecord<>(timeStamp, predicatedValue));
                System.out.println(timeStamp + " " + predicatedValue);
            }
        }
        LOGGER.log(Level.INFO, "Removing noise from input data using OLS regression" );
        return data;


    }

    private double[] estimateOLSRegressionParameters( Collection<TweetedMessageRecord<Number>> uploadedFile) {
        final OLSMultipleLinearRegression olsRegression = new OLSMultipleLinearRegression();
        double[] y = new double[1440];
        double[][] x2 = new double[1440][3];
        int i = 0;
        for (TweetedMessageRecord record : uploadedFile) {
            y[i] = 1.0 * (Long) record.getValue();
            x2[i][0] = 1;
            x2[i][1] = (double) record.getTimestamp();
            x2[i][2] = (double) record.getTimestamp() * record.getTimestamp();
            i += 1;
        }
        olsRegression.newSampleData(y, x2);
        olsRegression.setNoIntercept(true);
        olsRegression.newSampleData(y, x2);
        return olsRegression.estimateRegressionParameters();
    }

    private Collection<Long> extractTimestamps(Collection<TweetedMessageRecord<Number>> data) {
        Collection<Long> allTimestamps = new HashSet<>();
        for (TweetedMessageRecord record : data
                ) {
            allTimestamps.add(record.getTimestamp());
        }
        return allTimestamps;
    }


    private Collection<Long> computeAllTimestamps() {
        Collection<Long> allTimestamps = new HashSet<>();
        Long start = 0L;
        while (start <= 23) {
            for (Long i = 0L; i <= 59; i++) {
                allTimestamps.add(start * 100 + i);
            }
            start++;
        }


        return allTimestamps;
    }


    private int predictNextValue(List<TweetedMessage> uploadedFile, double timeSlot) throws ParseException {
        Collection<TweetedMessageRecord<Number>> data = MachineLearningInputTransformationHandler.tweetedMessageToRecordFormat(uploadedFile);

        final SimpleRegression simpleRegression = new SimpleRegression();
        final Iterator<TweetedMessageRecord<Number>> iterator = data.iterator();
        TweetedMessageRecord<Number> currentRecord;

        while (iterator.hasNext()) {
            currentRecord = iterator.next();
            simpleRegression.addData(currentRecord.getTimestamp(), currentRecord.getValue().doubleValue());
        }

        return (int) simpleRegression.predict(timeSlot);

    }

    private double predictNextValuesWithOLS(List<TweetedMessage> uploadedFile, double timeSlot) throws ParseException {
        Collection<TweetedMessageRecord<Number>> data = MachineLearningInputTransformationHandler.tweetedMessageToRecordFormat(uploadedFile);

        double[] olsRegressioParameters = estimateOLSRegressionParameters(data);
        return (olsRegressioParameters[0] + olsRegressioParameters[1] * timeSlot + olsRegressioParameters[2] * timeSlot * timeSlot);

    }
}