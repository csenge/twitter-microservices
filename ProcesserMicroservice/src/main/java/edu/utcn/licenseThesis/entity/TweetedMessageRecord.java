package edu.utcn.licenseThesis.entity;

/**
 * Created by cvarga on 6/7/2017.
 */
public class TweetedMessageRecord<T extends Number> {
    private final long timestamp;
    private final T value;

    public TweetedMessageRecord(long timestamp, T value) {
        super();
        this.timestamp = timestamp;
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public T getValue() {
        return value;
    }
}
