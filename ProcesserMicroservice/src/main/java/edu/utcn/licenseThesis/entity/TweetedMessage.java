package edu.utcn.licenseThesis.entity;


import java.util.Date;

/**
 * Created by csenge on 19.02.2017.
 */
public class TweetedMessage {

    private Integer id;
    private String tweetedText;
    private Date creationDate;
    private int favCount;
    private double latitude;
    private double longitude;
    private int retweetCount;
    private String retweetStatus;
    private int sentiment;
    private String hashtags;
    private String place;

    public TweetedMessage() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTweetedText() {
        return tweetedText;
    }

    public void setTweetedText(String tweetedText) {
        this.tweetedText = tweetedText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getFavCount() {
        return favCount;
    }

    public void setFavCount(int favCount) {
        this.favCount = favCount;
    }

    public int getRetweetCount() {
        return retweetCount;
    }

    public void setRetweetCount(int retweetCount) {
        this.retweetCount = retweetCount;
    }

    public String getRetweetStatus() {
        return retweetStatus;
    }

    public void setRetweetStatus(String retweetStatus) {
        this.retweetStatus = retweetStatus;
    }

    public int getSentiment() {
        return sentiment;
    }

    public void setSentiment(int sentiment) {
        this.sentiment = sentiment;
    }

    public String getHashtags() {
        return hashtags;
    }

    public void setHashtags(String hashtags) {
        this.hashtags = hashtags;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "TweetedMessage{" +
                "id=" + id +
                ", tweetedText='" + tweetedText + '\'' +
                ", creationDate=" + creationDate +
                ", favCount=" + favCount +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", retweetCount=" + retweetCount +
                ", retweetStatus='" + retweetStatus + '\'' +
                ", sentiment=" + sentiment +
                ", hashtags='" + hashtags + '\'' +
                ", place='" + place + '\'' +
                '}';
    }
}
