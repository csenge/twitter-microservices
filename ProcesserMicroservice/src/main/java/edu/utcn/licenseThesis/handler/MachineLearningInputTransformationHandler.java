package edu.utcn.licenseThesis.handler;

import edu.utcn.licenseThesis.entity.TweetedMessage;
import edu.utcn.licenseThesis.entity.TweetedMessageRecord;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by csenge on 29.06.2017.
 */
public class MachineLearningInputTransformationHandler {

    public static Collection<TweetedMessageRecord<Number>> tweetedMessageToRecordFormat(List<TweetedMessage> input) throws ParseException {
        List<TweetedMessageRecord<Number>> result = new ArrayList<>();
        List<String> allDates = countTweetNumber(input);
        Map<String, Long> countResult =
                allDates.stream().collect(
                        Collectors.groupingBy(
                                Function.identity(), Collectors.counting()
                        )
                );
        for (Map.Entry<String, Long> entry : countResult.entrySet())
        {
            result.add(new TweetedMessageRecord<>(Long.parseLong(entry.getKey()), entry.getValue()));
        }

        return result;
    }

    private static List<String> countTweetNumber(List<TweetedMessage> input) throws ParseException {
        List<String> allDates = new ArrayList<>();
        DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

        for (TweetedMessage line : input){
            allDates.add(new SimpleDateFormat("HHmm").format(line.getCreationDate()));
        }

        return allDates;
    }
}
