package edu.utcn.licenseThesis.handler;

import edu.utcn.licenseThesis.executor.OperationExecutor;

import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.TimeoutException;

/**
 * Created by csenge on 29.06.2017.
 */
public class RequestHandler {

    public String  processReqest(String requestMessage) throws Exception {

        OperationExecutor executor = new OperationExecutor();
        String[] requestMessageParts = requestMessage.split("#");

        switch (requestMessageParts[0]) {

            case "UploadFile":
               return executor.executeUploadFile(requestMessageParts[1], requestMessageParts[2]);
            case "GetPopularLocations":
               return executor.executeGetPopularLocations();
            case "GetPopularKeywords":
                return executor.getPopularKeywords();
            case "FilterByGeo":
               return executor.executeFilterByGeo(requestMessageParts[1], requestMessageParts[2], requestMessageParts[3]);
            case "filterBySentiment":
                return  executor.executeFilterBySentiment(requestMessageParts[1]);
            case "filterByKeyword":
                return executor.executeFilterByKeyword(requestMessageParts[1]);
            case "GetSentiment":
                return executor.executeGetSentiment();
            case "GetTendency":
                return String.valueOf(executor.executeGetTendency());
            case "RemoveNoise":
                String result = (requestMessageParts[1].equals("OLS")) ? executor.executeRemoveNoiseWithOLS() : executor.executeRemoveNoise();
                if (requestMessageParts[2].equals("true")){
                    //todo
                    executor.executeCrossValidation("");
                }
            return result;
            case "Predict":
                Long preditionFrame = Long.parseLong(requestMessageParts[1]);
               String resultPrediction = (requestMessageParts[2].equals("OLS")) ? executor.executePredictWithOLS(preditionFrame) : executor.executePredict(preditionFrame);
                return resultPrediction;
            case "CreateInputForDiagramCreation":
                executor.createInputForDiagramCreation(requestMessageParts[1]);
                return "SUCCESFUL DIAGRAM CREATION";
            default:
                System.out.println("Invalid");
                return "Invalid operation";

        }
    }
}
