package edu.utcn.licenseThesis.handler;

import edu.utcn.licenseThesis.entity.TweetedMessage;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by csenge on 29.06.2017.
 */
public class ReadFileHandler {

    public static List<TweetedMessage> manageReadFile(String message) throws ParseException {
        List<TweetedMessage> result = new ArrayList<>();

        TweetedMessage currentTweetedMessage = new TweetedMessage();
        int counter = 0;
        for (String messageComponent : message.split(",")
                ) {
            switch (counter) {
                case 0:
                    currentTweetedMessage.setId(Integer.parseInt(messageComponent));
                    counter++;
                    break;
                case 1:
                    currentTweetedMessage.setTweetedText(messageComponent);
                    counter++;
                    break;
                case 2:
                    currentTweetedMessage.setHashtags(messageComponent);
                    counter++;
                    break;

                case 3:
                    DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                    System.out.println(messageComponent);
                    Date date = format.parse(messageComponent);
                    currentTweetedMessage.setCreationDate(date);
                    counter++;
                    break;

                case 4:
                    Integer fav = ("null".equals( messageComponent)) ? -1 :  Integer.parseInt(messageComponent);
                    currentTweetedMessage.setFavCount(fav);
                    counter++;
                    break;

                case 5:
                    if (!messageComponent.equals("null")){
                        String[] geoComponents = messageComponent.split(" ");
                        currentTweetedMessage.setLatitude(Double.valueOf(geoComponents[0].substring(21, geoComponents[0].length())));
                        currentTweetedMessage.setLongitude(Double.valueOf(geoComponents[1].substring(10, geoComponents[1].length()-1)));
                    }else{
                        currentTweetedMessage.setLongitude(-1.0);
                        currentTweetedMessage.setLatitude(-1.0);
                    }
                    counter++;
                    break;
                case 6:
                    Integer count = ("null".equals( messageComponent)) ? -1 :  Integer.parseInt(messageComponent);
                    currentTweetedMessage.setRetweetCount(count);
                    counter++;
                    break;
                case 7:
                    currentTweetedMessage.setRetweetStatus(messageComponent);
                    counter++;
                    break;
                case 8:
                    Integer sentiment = ("null".equals( messageComponent)) ? -1 :  Integer.parseInt(messageComponent);
                    currentTweetedMessage.setSentiment(sentiment);
                    counter++;
                    break;
                case 9:
                    currentTweetedMessage.setPlace(messageComponent);
                    counter = 0;
                    result.add(currentTweetedMessage);
                    currentTweetedMessage = new TweetedMessage();
                    break;

            }


        }

        return result;
    }
}