package edu.utcn.licenseThesis;

import edu.utcn.licenseThesis.messaging.Server;

/**
 * Created by csenge on 25.06.2017.
 */
public class Starter {

    public static void main(String[] args){
        Server.waitForRequests();
    }
}
