package edu.utcn.licenseThesis.executor;

import edu.utcn.licenseThesis.entity.TweetedMessage;
import edu.utcn.licenseThesis.entity.TweetedMessageRecord;
import edu.utcn.licenseThesis.handler.ReadFileHandler;
import edu.utcn.licenseThesis.messaging.Client;
import edu.utcn.licenseThesis.service.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by csenge on 29.06.2017.
 */
public class OperationExecutor {

    Client readerClient = new Client("ioQueue");
    PopularityService popularityService = new PopularityService();
    MachineLearningService machineLearningService = new MachineLearningService();
    FilterService filterService = new FilterService();

    static List<TweetedMessage> uploadedFile = new ArrayList<>();

    public OperationExecutor() throws IOException, TimeoutException {
    }

    public String executeUploadFile(String location, String filename) throws IOException, InterruptedException, ParseException {
        String readfile = readerClient.call("Read," + location + "," + filename);
        uploadedFile = ReadFileHandler.manageReadFile(readfile);
        return readfile;
    }

    public String executeGetPopularLocations() {
        List<Object[]> keywordStatistics = popularityService.createKeywordStatisticsDataSet(uploadedFile, "location");
        return processStatistics(keywordStatistics);
    }

    public String getPopularKeywords() {
        List<Object[]> keywordStatistics = popularityService.createKeywordStatisticsDataSet(uploadedFile, "keyword");
        return processStatistics(keywordStatistics);
    }

    public String executeFilterByGeo(String latitude, String logitude, String radius) {
        List<Object[]> locationStatistics = filterService.filterByGeolocation(uploadedFile, latitude, logitude, radius);
        return processStatistics(locationStatistics);

    }

    public String executeFilterBySentiment(String sentimentValue) {
        List<Object[]> filterBySentiment = filterService.filterBySentiment(uploadedFile, sentimentValue);
        return processStatistics(filterBySentiment);

    }

    public String executeFilterByKeyword(String keyword) {
        List<Object[]> filterByKeyword = filterService.filterByKeyword(uploadedFile, keyword);
        return processStatistics(filterByKeyword);
    }

    public String executeGetSentiment() {
        List<Object[]> sentimentStatistics = popularityService.createKeywordStatisticsDataSet(uploadedFile, "sentiment");
        return processStatistics(sentimentStatistics);
    }


    public double executeGetTendency() throws ParseException {
        double trend = machineLearningService.computeRegressionTrend(uploadedFile);
        return trend;
    }

    public String executeRemoveNoise() throws ParseException, IOException, InterruptedException {
        Collection<TweetedMessageRecord<Number>> predictions = machineLearningService.removeNoise(uploadedFile);
        readerClient.call("Write,SimpleRegressionNoiseRemoval.csv,2,"+processPreditions(predictions));
        return processPreditions(predictions);
    }


    public String executePredict(Long preditionFrame) throws ParseException, IOException, InterruptedException {
        Collection<TweetedMessageRecord<Number>> predictions = machineLearningService.predictNextValuesInInterval(uploadedFile, preditionFrame);
        readerClient.call("Write,SimpleRegressionPrediction.csv,2,"+processPreditions(predictions));
        return processPreditions(predictions);
    }

    public String executeRemoveNoiseWithOLS() throws ParseException, IOException, InterruptedException {
        Collection<TweetedMessageRecord<Number>> predictions = machineLearningService.removeNoiseWithOLS(uploadedFile);
        readerClient.call("Write,OLSRegressionNoiseRemoval.csv,2,"+processPreditions(predictions));
        return processPreditions(predictions);
    }


    public String executePredictWithOLS(Long preditionFrame) throws ParseException, IOException, InterruptedException {
        Collection<TweetedMessageRecord<Number>> predictions = machineLearningService.predictNextValuesInIntervalWithOLS(uploadedFile, preditionFrame);
        readerClient.call("Write,OLSRegressionPrediction.csv,2,"+processPreditions(predictions));
        return processPreditions(predictions);
    }

    public void executeCrossValidation(String fileName) throws Exception {
        CrossValidatorService.crossValidate(fileName);
    }

    private String processStatistics(List<Object[]> statistics) {
        StringBuilder statisticsBuilder = new StringBuilder();
        for (Object[] record : statistics
                ) {
            statisticsBuilder.append(record[0] + " " + record[1] + ",");
        }
        return statisticsBuilder.toString();
    }

    private String processPreditions(Collection<TweetedMessageRecord<Number>> predictions) {
        StringBuilder predictionsBuilder = new StringBuilder();
        int i = 0;
        for (TweetedMessageRecord record : predictions
                ) {
            predictionsBuilder.append(i+ " " + record.getTimestamp() + " " + record.getValue() + ",");
            i++;
        }

        return predictionsBuilder.toString();
    }

    public void createInputForDiagramCreation(String frequency) throws IOException, InterruptedException {
        DiagramInputFormatService diagramService = new DiagramInputFormatService();
        String response = null;
        switch (frequency){
            case "1":
                String diagramByMinute = diagramService.formatWith1MinTimestamp(uploadedFile);
                 response = readerClient.call("Write,Diagram_1Min.csv,2,"+diagramByMinute);
                break;
            case "5":
                String diagramBy5Minutes = diagramService.formatWith5MinTimestamp(uploadedFile);
                response = readerClient.call("Write,Diagram_5Min.csv,2,"+diagramBy5Minutes);
                break;
            case "10":
                String diagramBy10Minutes = diagramService.formatWith10MinTimestamp(uploadedFile);
                response = readerClient.call("Write,Diagram_10Min.csv,2,"+diagramBy10Minutes);
                break;
            case "15":
                String diagramBy15Minutes = diagramService.formatWith15MinTimestamp(uploadedFile);
                response = readerClient.call("Write,Diagram_15Min.csv,2,"+diagramBy15Minutes);
                break;
            case "20":
                String diagramBy20Minutes = diagramService.formatWith20MinTimestamp(uploadedFile);
                response = readerClient.call("Write,Diagram_20Min.csv,2,"+diagramBy20Minutes);
                break;
            case "30":
                String diagramBy30Minutes = diagramService.formatWith30MinTimestamp(uploadedFile);
                response = readerClient.call("Write,Diagram_30Min.csv,2,"+diagramBy30Minutes);
                break;
            case "60":
                String diagramBy60Minutes = diagramService.formatWith60MinTimestamp(uploadedFile);
                response = readerClient.call("Write,Diagram_60Min.csv,2,"+diagramBy60Minutes);
                
        }
        System.out.println(response);

    }

}
